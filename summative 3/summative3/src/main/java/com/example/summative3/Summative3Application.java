package com.example.summative3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class Summative3Application {

	public static void main(String[] args) {
		SpringApplication.run(Summative3Application.class, args);
	}

}

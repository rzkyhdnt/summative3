package com.example.summative3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import javax.validation.ConstraintViolation;
import javax.validation.executable.ExecutableValidator;
import javax.validation.metadata.BeanDescriptor;
import java.time.LocalDate;
import java.util.Set;

@Component
public class LoginValidation implements Validator {
    @Autowired
    UserRepo userRepo;

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

        if(user.getBirthDate() == null){
            LocalDate date = LocalDate.parse("2020-01-01");
            user.setBirthDate(String.valueOf(date));
        }
    }
}

package com.example.summative3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    UserRepo userRepo;

    public List<User> getAllUser() {
        return userRepo.findAll();
    }

    @ModelAttribute
    public void searchEmail(Model model){
        List<User> users = new ArrayList<>(getAllUser());
        model.addAttribute("users", users);
    }

    @GetMapping("/")
    public String welcome(){
        return "homepage";
    }

    @GetMapping("/form")
    public String register(Model model){
        User user = new User();
        model.addAttribute("user", user);
        return "form";
    }

    @GetMapping("/welcome")
    public String login(){
        return "welcome";
    }

    @PostMapping("/success")
    public String createUser(@Valid @ModelAttribute("user") User user, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "form";
        } else {
            userRepo.save(user);
            return "success";
        }
    }

    @PostMapping("/welcome")
    public String validateLogin(String email, User user, RedirectAttributes model){
        try {
            User emails = userRepo.findByEmail(user.getEmail());
            User pass = userRepo.findByPassword(user.getPassword());
            String newEmail = null;
            String newPass = null;

            if(emails.getEmail() != null && pass.getPassword() != null){
                newEmail = emails.getEmail();
                newPass = pass.getPassword();
            } else {

            }

            if(newEmail != null || newPass != null) {
                if (email.equals(newEmail) && user.getPassword().equals(newPass)) {
                    return "welcome";
                } else {
                    return "redirect:/";
                }
            }else {
                model.addFlashAttribute("message", "Please input the password");
                return  "redirect:/";
            }
        } catch (Exception e){
            model.addFlashAttribute("message", "Email and Password incorect");
            return "redirect:/";
        }
    }
}

package com.example.summative3;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepo extends CrudRepository<User, Integer> {
    User findById(int id);
    User findByEmail(String email);
    User findByPassword(String password);
    List<User> findAll();
    void deleteById(int id);
    User save(User user);
}

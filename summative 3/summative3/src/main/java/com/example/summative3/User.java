package com.example.summative3;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotBlank(message = "*firstname can't be empty")
    private String firstName;
    @NotBlank(message = "*lastname can't be empty")
    private String lastName;
    @NotBlank(message = "*email can't be empty")
    @Email
    private String email;
    @NotBlank(message = "*phone number can't be empty")
    @Size(max = 12)
    private String phoneNumber;
    private String birthDate;
    private String gender;
    @NotBlank(message = "*password can't be empty")
    @Size(min = 6, max = 12, message = "*min 6 character, max 12 character")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$)$", message = "password minimal wajib memiliki 1 huruf besar, 1 special karakter, alfabet dan 1 angka. Minimal memiliki 6 karakter & max. 12")
    private String password;
    @Transient
    private String passwordRepeat;
    @Transient
    @AssertTrue(message = "Password not match, re-type your password")
    private boolean passwordEqual;

    public User(){
    }

    public User(int id, String firstName, String lastName, String email, String phoneNumber, String birthDate, String gender, String password, String passwordRepeat, boolean passwordEqual) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.birthDate = birthDate;
        this.gender = gender;
        this.password = password;
        this.passwordRepeat = passwordRepeat;
        this.passwordEqual = passwordEqual;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }


    public boolean isPasswordEqual() {
        return(password == null) ? false : password.equals(passwordRepeat);
    }

    public void setPasswordEqual(boolean passwordEqual) {
        this.passwordEqual = passwordEqual;
    }
}
